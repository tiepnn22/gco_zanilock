<?php
    // custom form comment
    comment_form(array(
        'label_submit'            =>  'Gửi',
        'title_reply'             =>  '',
        'comment_notes_before'    =>  '',
        'comment_notes_after'     =>  ''
    ));

    // list comment
    $cmt = get_comments(array(
        'post_id' => get_the_ID(),
        'status'  => 'approve',
        // 'number'=> 100
    ));
?>
<div class="form-comment-single">
    <?php
        if(!empty($cmt)) {
            echo '<ul>';

            foreach ($cmt as $cmt_kq) {

                $comment_link           = get_the_permalink($cmt_kq->comment_post_ID);
                $comment_id             = $cmt_kq->comment_ID;
                $comment_author_avatar  = get_avatar($cmt_kq->comment_author_email, 50);
                $comment_author         = $cmt_kq->comment_author;
                $comment_date           = $cmt_kq->comment_date;
                $comment_content        = $cmt_kq->comment_content;
                $comment_parent_id      = $cmt_kq->comment_parent;
            
                if($comment_parent_id == 0) { ?>

                    <li id="comment-<?php echo $comment_id; ?>">
                        <a href="<?php echo $comment_link; ?>#comment-<?php echo $comment_id; ?>">
                            <?php echo $comment_author_avatar; ?>
                        </a>
                        <div class="comment-info">
                            <a href="<?php echo $comment_link; ?>#comment-<?php echo $comment_id; ?>">
                                <?php echo $comment_author; ?>
                            </a>
                            <span> - <?php echo $comment_date; ?></span>
                            <div><br></div>
                            <div><?php echo $comment_content; ?></div>
                            <!-- <a class="comment-reply-link" onclick='return addComment.moveForm("div-comment-<?php echo $comment_id; ?>", "<?php echo $comment_id; ?>", "respond", "<?php echo get_the_ID(); ?>")' href="<?php echo $comment_link; ?>?replytocom=<?php echo $comment_id; ?>#respond">Trả lời</a> -->
                        </div>

                        <?php
                            // child comment
                            $cmt_child = get_comments(array( 
                                'parent' => $comment_id,
                                'status' => 'approve',
                                'hierarchical' => true
                            ));

                            if(!empty($cmt_child)) {
                                echo '<ul>';

                                foreach ($cmt_child as $cmt_child_kq) {

                                    $comment_link           = get_the_permalink($cmt_child_kq->comment_post_ID);
                                    $comment_id             = $cmt_child_kq->comment_ID;
                                    $comment_author_avatar  = get_avatar($cmt_child_kq->comment_author_email, 50);
                                    $comment_author         = $cmt_child_kq->comment_author;
                                    $comment_date           = $cmt_child_kq->comment_date;
                                    $comment_content        = $cmt_child_kq->comment_content;
                                    $comment_parent_id      = $cmt_child_kq->comment_parent;
                                    $comment_parent_author  = get_comment($comment_parent_id)->comment_author;
                                    $comment_parent_link    = get_the_permalink(get_comment($comment_parent_id)->comment_post_ID);
                                ?>

                                    <li id="comment-<?php echo $comment_id; ?>">
                                        <a href="<?php echo $comment_link; ?>#comment-<?php echo $comment_id; ?>">
                                            <?php echo $comment_author_avatar; ?>
                                        </a>
                                        <div class="comment-info">
                                            <a href="<?php echo $comment_link; ?>#comment-<?php echo $comment_id; ?>">
                                                <?php echo $comment_author; ?>
                                            </a>
                                            <span> - <?php echo $comment_date; ?></span>
                                            <div>
                                                Trả lời cho 
                                                <a href="<?php echo $comment_parent_link; ?>#comment-<?php echo $comment_parent_id; ?>">
                                                    <?php echo $comment_parent_author; ?>
                                                </a>
                                            </div>
                                            <div><br></div>
                                            <div><?php echo $comment_content; ?></div>
                                            <!-- <a class="comment-reply-link" onclick='return addComment.moveForm("div-comment-<?php echo $comment_id; ?>", "<?php echo $comment_id; ?>", "respond", "<?php echo get_the_ID(); ?>")' href="<?php echo $comment_link; ?>?replytocom=<?php echo $comment_id; ?>#respond">Trả lời</a> -->
                                        </div>
                                    </li>

                                <?php
                                }
                                echo '</ul>';
                            }
                    echo '</li>';
                }
            }
            echo '</ul>';
        }
    ?>
</div>


<style type="text/css">
    html { scroll-behavior: smooth; }

    /*list comment*/
    .form-comment-single { clear: both; }
    .form-comment-single ul { padding: 0; margin: 0; list-style-type: none; }
    .form-comment-single ul ul { padding: 0 0 0 65px; }
    .form-comment-single ul li { display: inline-block; width: 100%; }
    .form-comment-single ul li > a { display: inline-block; float: left; width: 65px; }
    .form-comment-single ul li .comment-info {
        float: left;
        margin: 0 0 15px 0;
        border: 1px solid #e4e1e3;
        padding: 10px 15px;
        width: calc(100% - 65px);
    }

    /*form*/
    .comment-form label {
        font-size: 20px;
        font-weight: bold;
    }
    .comment-form input {
        border: 1px solid #e6e6e6;
        height: 45px;
        margin-bottom: 25px;
        padding: 0 15px;
        width: 100%;
        position: relative;
    }
    .comment-form textarea {
        border: 1px solid #e6e6e6;
        height: 130px;
        margin-bottom: 25px;
        padding: 15px;
        width: 100%;
    }
    .comment-form input[type=checkbox] {
        height: auto;
        margin-bottom: 0;
        padding: 0;
        width: auto;
    }
    .comment-form input[type=submit] {
        cursor: pointer;
        float: right;
        width: auto;
        margin-bottom: 35px;
    }
</style>