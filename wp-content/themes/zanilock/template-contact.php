<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );

    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map = get_field('contact_map');

    $contact_contact_title = get_field('contact_contact_title');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="ec-common-wrapper">
                <div class="ec-contact-leftside">
                    <div class="ec-contact-container">
                        <div class="ec-contact-form">

                            <?php if(!empty( $contact_contact_form )) { ?>
                            <!-- <div class="vk-form--contact"> -->
                                <?php echo $contact_contact_form; ?>
                            <!-- </div> -->
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="ec-contact-rightside">
                    <div class="ec_contact_map">
                        <div class="ec_map_canvas">
                            <?php echo $contact_map; ?>
                        </div>
                    </div>
                    <div class="ec_contact_info">
                        <h1 class="ec_contact_info_head"><?php echo $contact_contact_title; ?></h1>
                        <ul class="align-items-center">
                            <li class="ec-contact-item"><i class="ecicon eci-map-marker" aria-hidden="true"></i>
                                <span>Địa chỉ :</span><?php echo $customer_address; ?>
                            </li>
                            <li class="ec-contact-item align-items-center"><i class="ecicon eci-phone" aria-hidden="true"></i>
                                <span>Số điện thoại:</span><a href="tel:<?php echo str_replace(' ','',$customer_phone);?>"><?php echo $customer_phone; ?></a>
                            </li>
                            <li class="ec-contact-item align-items-center"><i class="ecicon eci-envelope" aria-hidden="true"></i>
                                <span>Email :</span><a href="mailto:<?php echo $customer_email; ?>"><?php echo $customer_email; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>