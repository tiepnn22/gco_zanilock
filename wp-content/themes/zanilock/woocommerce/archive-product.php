<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info         = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id           = $term_info->term_id;
    $term_name_check   = $term_info->name;
    $term_name		   = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    $term_excerpt      = wpautop($term_info->description);
    // $term_link      = esc_url(get_term_link($term_id));
    // $taxonomy_name  = $term_info->taxonomy;
    $thumbnail_id   = get_term_meta( $term_id, 'thumbnail_id', true );
    $term_image     = wp_get_attachment_url( $thumbnail_id );

    //banner
    // $data_page_banner  = array(
    //     'image_alt'    =>    $term_name
    // );
?>

<div class="sticky-header-next-sec  ec-breadcrumb section-space-mb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row ec_breadcrumb_inner">
                    <div class="col-md-6 col-sm-12">
                        <h2 class="ec-breadcrumb-title"><?php echo $term_name; ?></h2>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="ec-breadcrumb-list">
                            <?php
                                if(function_exists('bcn_display')) {
                                    bcn_display(); 
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!empty( $term_id )) { ?>
<section class="ec-bnr-detail margin-bottom-30 section-space-pt">
    <div class="ec-page-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="ec-cat-bnr">
                        <?php if(!empty( $term_image )) { ?>
                            <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="ec-page-description">
                        <h6><?php echo $term_name; ?></h6>
                        <p class="m-0"><?php echo $term_excerpt; ?><p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>


<?php
// do_action( 'woocommerce_before_main_content' );
?>
<!-- <header class="woocommerce-products-header">
    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
        <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
    <?php endif; ?>

    <?php
        do_action( 'woocommerce_archive_description' );
    ?>
</header> -->


<section class="ec-page-content-bnr section-space-pb">
    <div class="container">
        <div class="row">

            <div class="ec-shop-rightside col-lg-9 order-lg-last col-md-12 order-md-first margin-b-30">
                <div class="ec-pro-list-top d-flex">
                    <div class="col-md-6 ec-grid-list">
                        <div class="ec-gl-btn">
                            <button class="btn btn-grid active">
                                <img src="<?php echo core_asset('images/icons/grid.svg'); ?>" class="svg_img gl_svg" alt="" />
                            </button>
                            <button class="btn btn-list">
                                <img src="<?php echo core_asset('images/icons/list.svg'); ?>" class="svg_img gl_svg" alt="" />
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6 ec-sort-select">

                        <!-- order -->
                        <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                    </div>
                </div>

                <div class="shop-pro-content">
                    <div class="shop-pro-inner">
                        <div class="row"> 

                            <?php
                                if ( woocommerce_product_loop() ) {

                                    //do_action( 'woocommerce_before_shop_loop' );

                                    // woocommerce_product_loop_start();
                                    if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            // do_action( 'woocommerce_shop_loop' );
                                            // wc_get_template_part( 'content', 'product' );
                                            get_template_part('resources/views/content/category-product', get_post_format());
                                        }
                                    }
                                    // woocommerce_product_loop_end();

                                    // pagination
                                    do_action( 'woocommerce_after_shop_loop' );
                                } else {
                                    do_action( 'woocommerce_no_products_found' );
                                }
                            ?>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="ec-shop-leftside col-lg-3 order-lg-first col-md-12 order-md-last">
                <div id="shop_sidebar">
                    <div class="ec-sidebar-heading">
                        <h1>Tìm kiếm sản phẩm</h1>
                    </div>

                    <div class="ec-sidebar-wrap">
                        <?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
// do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
