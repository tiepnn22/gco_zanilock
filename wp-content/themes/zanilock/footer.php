<?php
    //field
    $select_page_wishlist = get_field('select_page_wishlist', 'option');
    $wishlist_count_item  = YITH_WCWL()->count_all_products();

    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $f_messenger = get_field('f_messenger', 'option');

    $f_form_title   = get_field('f_form_title', 'option');
    $f_form_desc    = get_field('f_form_desc', 'option');
    $f_form_id      = get_field('f_form', 'option');
    $f_form         = do_shortcode('[contact-form-7 id="'.$f_form_id.'"]');

    $f_bottom_copyright = get_field('f_bottom_copyright', 'option');

    $f_card_image = get_field('f_card_image', 'option');
?>


<?php
    if( !is_front_page() ){
?>
    <footer class="ec-footer section-space-mt">
        <div class="footer-container">
            <div class="footer-offer">
                <div class="container">
                    <div class="row">
                        <div class="text-center footer-off-msg">
                            <span><?php echo $f_messenger; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-top section-space-footer-p">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3 ec-footer-contact">
                            <div class="ec-footer-widget">

                                <!-- logo footer -->
                                <?php get_template_part("resources/views/logo-footer"); ?>

                                <div class="ec-footer-links">
                                    <ul class="align-items-center">
                                        <li class="ec-footer-link"><?php echo $customer_address; ?></li>
                                        <li class="ec-footer-link"><span>Số điện thoại:</span>
                                            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>"><?php echo $customer_phone; ?></a></li>
                                        <li class="ec-footer-link"><span>Email:</span>
                                            <a href="mailto:<?php echo $customer_email; ?>"><?php echo $customer_email; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-info">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-info" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-info',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-account">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-account" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-account',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-service">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-services" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-services',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-3 ec-footer-news">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo $f_form_title; ?></h4>
                                <div class="ec-footer-links">
                                    <ul class="align-items-center">
                                        <li class="ec-footer-link"><?php echo $f_form_desc; ?></li>
                                    </ul>
                                    <div class="ec-subscribe-form">

                                        <?php if(!empty( $f_form )) { ?>
                                        <div  id="ec-newsletter-form">
                                            <?php echo $f_form; ?>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col text-left footer-bottom-left">
                            <div class="footer-bottom-social">
                                <!-- <span class="social-text text-upper">Follow us on:</span> -->

                                <!-- socical-customer -->
                                <?php get_template_part("resources/views/socical-customer"); ?>
                            </div>
                        </div>
                        <div class="col text-center footer-copy">
                            <div class="footer-bottom-copy ">
                                <div class="ec-copy"><?php echo $f_bottom_copyright; ?></div>
                            </div>
                        </div>
                        <div class="col footer-bottom-right">
                            <div class="footer-bottom-payment d-flex justify-content-end">
                                <div class="payment-link">
                                    <img src="<?php echo $f_card_image; ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- mobile footer toolbar -->
    <div class="ec-nav-toolbar">
        <div class="container">
            <div class="ec-nav-panel">
                <div class="ec-nav-panel-icons">
                    <a href="#ec-mobile-menu" class="navbar-toggler-btn ec-header-btn ec-side-toggle"><img src="<?php echo core_asset('images/icons/menu.svg'); ?>" class="svg_img header_svg" alt="" /></a>
                </div>
                <div class="ec-nav-panel-icons">
                    <a href="#ec-side-cart" class="toggle-cart ec-header-btn ec-side-toggle"><img src="<?php echo core_asset('images/icons/cart.svg'); ?>" class="svg_img header_svg" alt="" />
                        <span class="ec-cart-noti ec-header-count cart-count-lable">
                            <?php
                                $cart_count    = WC()->cart->get_cart_contents_count();
                                if($cart_count > 0) { echo $cart_count; } else { echo 0; }
                            ?>
                        </span></a>
                </div>
                <div class="ec-nav-panel-icons">
                    <a href="<?php echo get_option('home');?>" class="ec-header-btn">
                        <img src="<?php echo core_asset('images/icons/home.svg'); ?>" class="svg_img header_svg" alt="icon" />
                    </a>
                </div>
                <div class="ec-nav-panel-icons">
                    <a href="<?php echo $select_page_wishlist; ?>" class="ec-header-btn">
                        <img src="<?php echo core_asset('images/icons/wishlist.svg'); ?>" class="svg_img header_svg" alt="icon" />
                        <span class="ec-cart-noti"><?php echo $wishlist_count_item; ?></span>
                    </a>
                </div>
                <div class="ec-nav-panel-icons">
                    <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="ec-header-btn">
                        <img src="<?php echo core_asset('images/icons/user.svg'); ?>" class="svg_img header_svg" alt="icon" />
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php
    } else {
?>
    <footer class="ec-footer">
        <div class="footer-container">
            <div class="footer-offer">
                <div class="container">
                    <div class="row">
                        <div class="text-center footer-off-msg">
                            <span><?php echo $f_messenger; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-top section-space-footer-p">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3 ec-footer-contact">
                            <div class="ec-footer-widget">

                                <!-- logo footer -->
                                <?php get_template_part("resources/views/logo-footer"); ?>

                                <h4 class="ec-footer-heading">Địa chỉ</h4>
                                <div class="ec-footer-links">
                                    <ul class="align-items-center">
                                        <li class="ec-footer-link"><?php echo $customer_address; ?></li>
                                    </ul>
                                    <div class="col-sm-12 col-lg-3 ec-footer-social">
                                        <div class="ec-footer-widget">
                                            <div class="ec-footer-links">
                                                <!-- socical-customer -->
                                                <?php get_template_part("resources/views/socical-customer"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-3 ec-footer-news">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo $f_form_title; ?></h4>
                                <div class="ec-footer-links">
                                    <ul class="align-items-center">
                                        <li class="ec-footer-link"><?php echo $f_form_desc; ?></li>
                                    </ul>
                                    <div class="ec-subscribe-form">

                                        <?php if(!empty( $f_form )) { ?>
                                        <div  id="ec-newsletter-form">
                                            <?php echo $f_form; ?>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-account">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-account" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-account',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-service">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-services" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-services',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-2 ec-footer-info">
                            <div class="ec-footer-widget">
                                <h4 class="ec-footer-heading"><?php echo wp_get_nav_menu_name("f-info" ); ?></h4>
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'f-info',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-footer-links',
                                            'container_id'      =>  '',
                                            'menu_class'        =>  'align-items-center',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col footer-copy">
                            <div class="footer-bottom-copy ">
                                <div class="ec-copy"><?php echo $f_bottom_copyright; ?></div>
                            </div>
                        </div>
                        <div class="col footer-bottom-right">
                            <div class="footer-bottom-payment d-flex justify-content-end">
                                <div class="payment-link">
                                    <img src="<?php echo $f_card_image; ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php
    }
?>


<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>

<?php echo get_field('insert_code_footer', 'option'); ?>
</body>
</html>