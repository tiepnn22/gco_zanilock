<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()

	//banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="page-page">
    <div class="container">

		<div class="title-section">
			<h1><?php echo $page_name; ?></h1>
		</div>
		<div class="<?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
            <?php the_content(); ?>
        </div>

    </div>
</section>

<?php get_footer(); ?>