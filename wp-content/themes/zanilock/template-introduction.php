<?php
	/*
	Template Name: Mẫu Giới thiệu
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );

    //field
    $intro_intro_section_title  = get_field('intro_intro_section_title');
    $intro_intro_section_desc   = get_field('intro_intro_section_desc');
    $intro_intro_image          = get_field('intro_intro_image');
    $intro_intro_title          = get_field('intro_intro_title');
    $intro_intro_desc           = get_field('intro_intro_desc');

    $intro_testimonial_section_title    = get_field('intro_testimonial_section_title');
    $intro_testimonial_section_desc     = get_field('intro_testimonial_section_desc');
    $intro_testimonial_content          = get_field('intro_testimonial_content');

    $intro_service_content = get_field('intro_service_content');

    $intro_instagram_section_title  = get_field('intro_instagram_section_title');
    $intro_instagram_section_desc   = get_field('intro_instagram_section_desc');
    $intro_instagram_content        = get_field('intro_instagram_content');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $intro_intro_section_title; ?></h2>
                    <p class="sub-title mb-3"><?php echo $intro_intro_section_desc; ?></p>
                </div>
            </div>
            <div class="ec-common-wrapper">
                <div class="row">
                    <div class="col-md-6 ec-cms-block ec-abcms-block text-center">
                        <div class="ec-cms-block-inner">
                            <img class="a-img" src="<?php echo $intro_intro_image; ?>" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 ec-cms-block ec-abcms-block text-center">
                        <div class="ec-cms-block-inner">
                            <h3 class="ec-cms-block-title"><?php echo $intro_intro_title; ?></h3>
                            <p><?php echo $intro_intro_desc; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section ec-test-section section-space-ptb-100 section-space-m">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title mb-0">
                    <h2 class="ec-title"><?php echo $intro_testimonial_section_title; ?></h2>
                    <p class="sub-title mb-3"><?php echo $intro_testimonial_section_desc; ?></p>
                </div>
            </div>
        </div>

        <?php if(!empty( $intro_testimonial_content )) { ?>
        <div class="row">
            <div class="ec-test-outer">
                <ul id="ec-testimonial-slider">

                    
                    <?php
                        foreach ($intro_testimonial_content as $foreach_kq) {

                        $post_image = $foreach_kq["image"];
                        $post_title = $foreach_kq["title"];
                        $post_job   = $foreach_kq["job"];
                        $post_star  = $foreach_kq["star"];
                        $post_desc  = $foreach_kq["desc"];
                    ?>
                        <li class="ec-test-item">
                            <img src="<?php echo core_asset('images/testimonial/top-quotes.svg'); ?>" class="svg_img test_svg top" alt="" />
                            <div class="ec-test-inner">
                                <div class="ec-test-img">
                                    <img alt="testimonial" title="testimonial" src="<?php echo $post_image; ?>" />
                                </div>
                                <div class="ec-test-content">
                                    <div class="ec-test-desc"><?php echo $post_desc; ?></div>
                                    <div class="ec-test-name"><?php echo $post_title; ?></div>
                                    <div class="ec-test-designation"><?php echo $post_job; ?></div>
                                    <div class="ec-test-rating">
                                        <?php
                                            for ($i = 0; $i < $post_star; $i++){
                                                echo '<i class="ecicon eci-star fill"></i>';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <img src="<?php echo core_asset('images/testimonial/bottom-quotes.svg'); ?>" class="svg_img test_svg bottom" alt="" />
                        </li>
                    <?php } ?>
                    
                </ul>
            </div>
        </div>
        <?php } ?>

    </div>
</section>

<?php if(!empty( $intro_service_content )) { ?>
<section class="section ec-services-section section-space-p">
    <div class="container">
        <div class="row">

            <?php
                foreach ($intro_service_content as $foreach_kq) {

                $post_image = $foreach_kq["image"];
                $post_title = $foreach_kq["title"];
                $post_desc  = $foreach_kq["desc"];
            ?>
                <div class="ec_ser_content ec_ser_content_1 col-sm-12 col-md-6 col-lg-3">
                    <div class="ec_ser_inner">
                        <div class="ec-service-image">
                            <img src="<?php echo $post_image; ?>" class="svg_img" alt="" />
                        </div>
                        <div class="ec-service-desc">
                            <h2><?php echo $post_title; ?></h2>
                            <p><?php echo $post_desc; ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="section ec-instagram-section module section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $intro_instagram_section_title; ?></h2>
                    <p class="sub-title"><?php echo $intro_instagram_section_desc; ?></p>
                </div>
            </div>
        </div>
    </div>

    <?php if(!empty( $intro_instagram_content )) { ?>
    <div class="ec-insta-wrapper">
        <div class="ec-insta-outer">
            <div class="container">
                <div class="insta-auto">

                    <?php
                        foreach ($intro_instagram_content as $foreach_kq) {

                        $post_image = $foreach_kq["image"];
                        $post_link  = $foreach_kq["url"];
                    ?>
                        <div class="ec-insta-item">
                            <div class="ec-insta-inner">
                                <a href="<?php echo $post_link; ?>" target="_blank">
                                    <img src="<?php echo $post_image; ?>" alt="insta">
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>

<?php get_footer(); ?>


<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/dist/js/plugins/infiniteslidev2.js'; ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(function(){
            $('.insta-auto, .cat-auto').infiniteslide({
                direction: 'left',
                speed: 50,
                clone: 10
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    });
</script>