<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo core_get_page_link_current(); ?>" />
    <title><?php echo core_title(); ?></title>
    <?php wp_head(); ?>

    <?php echo get_field('insert_code_header', 'option'); ?>
</head>
<body <?php body_class(); ?>>


<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>


<?php
    //field
    $h_messenger        = get_field('h_messenger', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
?>


<div id="ec-overlay"><span class="loader_img"></span></div>


<?php
    if( !is_front_page() ){
?>
    <header class="ec-header">
        <div class="header-top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col text-left header-top-left d-none d-lg-block">
                        <div class="header-top-social">
                            <!-- socical-customer -->
                            <?php get_template_part("resources/views/socical-customer"); ?>
                        </div>
                    </div>
                    <div class="col text-center header-top-center">
                        <div class="header-top-message text-upper">
                            <span><?php echo $h_messenger; ?></span>
                        </div>
                    </div>
                    <div class="col header-top-right d-none d-lg-block">
                        <div class="header-top-lan-curr d-flex justify-content-end">
                            <!-- Currency Start --><!-- Currency End -->
                            <!-- Language Start --><!-- Language End -->
                        </div>
                    </div>

                    <!--mobile-->
                    <div class="col d-lg-none">
                        <!-- header-right -->
                        <?php get_template_part("resources/views/header-right"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ec-header-bottom d-none d-lg-block">
            <div class="container position-relative">
                <div class="row">
                    <div class="ec-flex">
                        <div class="align-self-center">
                            <!-- logo -->
                            <?php get_template_part("resources/views/logo"); ?>
                        </div>
                        <div class="align-self-center">
                            <!-- search -->
                            <?php get_template_part("resources/views/search-form"); ?>
                        </div>
                        <div class="align-self-center">
                            <!-- header-right -->
                            <?php get_template_part("resources/views/header-right"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--mobile-->
        <div class="ec-header-bottom d-lg-none">
            <div class="container position-relative">
                <div class="row ">
                    <div class="col">
                        <!-- logo -->
                        <?php get_template_part("resources/views/logo"); ?>
                    </div>
                    <div class="col">
                        <!-- search -->
                        <?php get_template_part("resources/views/search-form"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="ec-main-menu-desk" class="d-none d-lg-block sticky-nav">
            <div class="container position-relative">
                <div class="row">
                    <div class="col-md-12 align-self-center">
                        <div class="ec-main-menu">
                            <!-- menu -->
                            <?php get_template_part("resources/views/menu"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--mobile-->
        <div id="ec-mobile-menu" class="ec-side-cart ec-mobile-menu">
            <div class="ec-menu-title">
                <span class="menu_title">Menu</span>
                <button class="ec-close">×</button>
            </div>
            <div class="ec-menu-inner">
                <div class="ec-menu-content">
                    <!-- menu -->
                    <?php get_template_part("resources/views/menu"); ?>
                </div>
                <div class="header-res-lan-curr">
                    <div class="header-res-social">
                        <div class="header-top-social">
                            <!-- socical-customer -->
                            <?php get_template_part("resources/views/socical-customer"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php
    } else {
?>
    <header class="ec-header">
        <div class="header-top">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col header-top-left">
                        <div class="header-top-call">
                            <img src="<?php echo core_asset('images/icons/top-call.svg'); ?>" class="svg_img top_svg" alt="" /> 
                            Số điện thoại: 
                            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>"><?php echo $customer_phone; ?></a>
                        </div>
                    </div>
                    <div class="col header-top-center">
                        <div class="header-top-call">
                            <?php echo $h_messenger; ?>
                        </div>
                    </div>
                    <div class="col header-top-right d-none d-lg-block">
                        <div class="header-top-right-inner d-flex justify-content-end">
                            <div class="header-top-social">
                                <!-- socical-customer -->
                                <?php get_template_part("resources/views/socical-customer"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col header-top-res d-lg-none">
                        <!-- header-right -->
                        <?php get_template_part("resources/views/header-right"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ec-header-bottom d-none d-lg-block">
            <div class="container position-relative">
                <div class="row">
                    <div class="header-bottom-flex">
                        <div class="align-self-center ec-header-logo ">
                            <!-- logo -->
                            <?php get_template_part("resources/views/logo"); ?>
                        </div>
                        <div class="align-self-center ec-header-search">
                            <!-- logo -->
                            <?php get_template_part("resources/views/search-form-by-category"); ?>
                        </div>
                        <div class="align-self-center ec-header-cart">
                            <!-- header-right -->
                            <?php get_template_part("resources/views/header-right"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ec-header-bottom d-lg-none">
            <div class="container position-relative">
                <div class="row ">
                    <div class="col">
                        <!-- logo -->
                        <?php get_template_part("resources/views/logo"); ?>
                    </div>
                    <div class="col align-self-center ec-header-search">
                        <!-- logo -->
                        <?php get_template_part("resources/views/search-form-by-category"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="ec-main-menu-desk" class="sticky-nav">
            <div class="container position-relative">
                <div class="row">
                    <div class="col-sm-2 ec-category-block">
                        <div class="ec-category-menu">
                            <div class="ec-category-toggle">
                                <span class="ec-category-title">
                                    <?php echo wp_get_nav_menu_name("h-category" ); ?>
                                </span>
                                <i class="ecicon eci-angle-down" aria-hidden="true"></i>
                            </div>
                            <div class="ec-category-content">
                                <?php
                                    if(function_exists('wp_nav_menu')){
                                        $args = array(
                                            'theme_location'    =>  'h-category',
                                            'container'         =>  'div',
                                            'container_class'   =>  'ec-category-menu',
                                            'container_id'      =>  'ec-category-menu',
                                            'menu_class'        =>  'ec-category-wrapper',
                                        );
                                        wp_nav_menu( $args );
                                    }
                                ?>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10 ec-main-menu-block align-self-center d-none d-lg-block">
                        <div class="ec-main-menu">
                            <!-- menu -->
                            <?php get_template_part("resources/views/menu"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ec-mobile-menu" class="ec-side-cart ec-mobile-menu">
            <div class="ec-menu-title">
                <span class="menu_title">Menu</span>
                <button class="ec-close">×</button>
            </div>
            <div class="ec-menu-inner">
                <div class="ec-menu-content">
                    <!-- menu -->
                    <?php get_template_part("resources/views/menu"); ?>
                </div>
                <div class="header-res-lan-curr">
                    <div class="header-top-lan-curr">
                        <!-- Language Start --><!-- Language End -->
                        <!-- Currency Start --><!-- Currency End -->
                    </div>
                    <div class="header-res-social">
                        <div class="header-top-social">
                            <!-- socical-customer -->
                            <?php get_template_part("resources/views/socical-customer"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php
    }
?>


<!-- cart mini -->
<?php get_template_part("resources/views/wc/wc-info-cart"); ?>