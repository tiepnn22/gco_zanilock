<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var('category_name') );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_desc 		= wpautop( category_description($cat_id) );
	$cat_link 		= esc_url( get_term_link($cat_id) );
	
	//banner
	$data_page_banner  = array(
		'image_alt'    =>    $cat_name
	);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="ec-blogs-rightside col-lg-12 col-md-12">

                <div class="ec-blogs-content">
                    <div class="ec-blogs-inner">
                        <div class="row">

                            <?php
                                $query = core_query_post_by_category_paged($cat_id, 6);
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $max_num_pages = $query->max_num_pages;
                                $total_post         = $query->found_posts;
                                $total_post_start   = ($paged -1) * 6 + 1;
                                $total_post_end     = min( $total_post, $paged * 6 );

                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>
                    </div>

                    <!-- pagination -->
                    <div class="ec-pro-pagination">
                        <span>Hiển thị <?php echo $total_post_start; ?> - <?php echo $total_post_end; ?> / <?php echo $total_post; ?> kết quả</span>
                        <?php echo core_paginationCustom( $max_num_pages ); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>