<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_date 		= get_the_date('d-m-Y', $post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= core_getPostImage($post_id,"full");
	$single_post_excerpt 	= get_the_excerpt($post_id);
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
    $single_post_comment       	= wp_count_comments($post_id);
    $single_post_comment_total 	= $single_post_comment->total_comments;

	//banner
	$data_page_banner  = array(
		'image_alt'    =>    $cat_name
	);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="ec-blogs-rightside col-lg-12 col-md-12">
                <div class="ec-blogs-content">
                    <div class="ec-blogs-inner">
                        <div class="ec-blog-main-img">
                            <img class="blog-image" src="<?php echo $single_post_image; ?>" alt="<?php echo $single_post_title; ?>" />
                        </div>
                        <div class="ec-blog-date">
                            <p class="date"><?php echo $single_post_date; ?> - </p>
                            <a href="javascript:void(0)"><?php echo $single_post_comment_total; ?> Bình luận</a>
                        </div>
                        <div class="ec-blog-detail wp-editor-fix">
                    		<?php the_content(); ?>
                        </div>

                        <div class="ec-blog-tags">
							<?php
								if(!empty($single_post_tag)) {
							    foreach ($single_post_tag as $single_post_tag_kq) {

							    $post_id 	= $single_post_tag_kq->term_id;
							    $post_title = $single_post_tag_kq->name;
							    $post_link 	= get_term_link($post_id);
							?>
								<a href="<?php echo $post_link; ?>">
							    	<?php echo $post_title; ?>
							    	<span class="separator">,</span>
								</a>
							<?php } } ?>
                        </div>

                        <?php get_template_part("resources/views/socical-bar"); ?>

						<?php get_template_part("resources/views/previous-post-next-post"); ?>

                        <?php
                            if ( comments_open() || get_comments_number() ) {
                                comments_template();
                            }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part("resources/views/template-related-post"); ?>

<?php get_footer(); ?>