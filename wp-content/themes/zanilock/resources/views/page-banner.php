<?php
	global $data_page_banner;
	
	if(!empty( $data_page_banner )) {
		// $image_link_check 	= $data_page_banner['image_link'];
		// $image_link 		= (!empty($image_link_check)) ? $image_link_check : core_asset('images/2x1.png');
		$image_alt 			= $data_page_banner['image_alt'];
	}
?>

<div class="sticky-header-next-sec  ec-breadcrumb section-space-mb">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row ec_breadcrumb_inner">
                    <div class="col-md-6 col-sm-12">
                        <h2 class="ec-breadcrumb-title"><?php echo $image_alt; ?></h2>
                    </div>

                    <div class="col-md-6 col-sm-12">
					    <div class="ec-breadcrumb-list">
					        <?php
					            if(function_exists('bcn_display')) {
					                bcn_display(); 
					            }
					        ?>
					    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>