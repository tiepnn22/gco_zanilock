<?php
    //field
    $select_page_wishlist = get_field('select_page_wishlist', 'option');
    $wishlist_count_item  = YITH_WCWL()->count_all_products();
?>

<div class="ec-header-bottons">
    <div class="ec-header-user dropdown">
        <button class="dropdown-toggle" data-bs-toggle="dropdown">
            <img src="<?php echo core_asset('images/icons/user.svg'); ?>" class="svg_img header_svg" alt="" />
            <span class="d-none ec-btn-title">Tài khoản</span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Tài khoản</a></li>
            <li><a class="dropdown-item" href="<?php echo site_url('/wp-login.php?action=register'); ?>">Đăng ký</a></li>
            <li><a class="dropdown-item" href="<?php echo wp_login_url(); ?>">Đăng nhập</a></li>
        </ul>
    </div>
    <a href="<?php echo $select_page_wishlist; ?>" class="ec-header-btn ec-header-wishlist">
        <div class="header-icon">
            <img src="<?php echo core_asset('images/icons/wishlist.svg'); ?>" class="svg_img header_svg" alt="" />
        </div>
        <span class="ec-header-count"><?php echo $wishlist_count_item; ?></span>
        <span class="d-none ec-btn-title">Yêu thích</span>
    </a>
    <a href="#ec-side-cart" class="ec-header-btn ec-side-toggle">
        <div class="header-icon">
            <img src="<?php echo core_asset('images/icons/cart.svg'); ?>" class="svg_img header_svg" alt="" />
        </div>
        <span class="ec-header-count cart-count-lable">
            <?php
                $cart_count    = WC()->cart->get_cart_contents_count();
                if($cart_count > 0) { echo $cart_count; } else { echo 0; }
            ?>
        </span>
        <span class="d-none ec-btn-title">Giỏ hàng</span>
    </a>
    <a href="#ec-mobile-menu" class="ec-header-btn ec-side-toggle d-lg-none">
        <img src="<?php echo core_asset('images/icons/menu.svg'); ?>" class="svg_img header_svg" alt="icon" />
    </a>
</div>