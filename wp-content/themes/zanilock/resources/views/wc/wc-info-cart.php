<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="ec-side-cart-overlay"></div>
<div id="ec-side-cart" class="ec-side-cart">
    <div class="ec-cart-inner">

        <?php if($cart_count > 0) { ?>

            <div class="ec-cart-top">
                <div class="ec-cart-title">
                    <span class="cart_title">Giỏ hàng của bạn</span>
                    <button class="ec-close">×</button>
                </div>
                <ul class="eccart-pro-items">

                    <?php
                        foreach ($info_cart as $info_cart_kq) {
                            
                        $product_id         = $info_cart_kq["product_id"];
                        $product_title      = get_the_title($product_id);
                        $product_link       = get_permalink($product_id);
                        $product_image      = core_getPostImage($product_id,"p-product");

                        $product_quantity   = $info_cart_kq["quantity"];
                        $product_total      = $info_cart_kq["line_total"];
                        $product_price      = core_format_price_donvi($product_total / $product_quantity);
                    ?>

                        <li class="single-cart-item">
                            <a href="<?php echo $product_link; ?>" class="sidekka_pro_img">
                                <img src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>">
                            </a>
                            <div class="ec-pro-content">
                                <a href="<?php echo $product_link; ?>" class="cart_pro_title">
                                    <?php echo $product_title; ?>
                                </a>
                                <span class="cart-price">
                                    <span>
                                        <?php echo core_show_price_old_price($product_id); ?>
                                    </span> x <?php echo $product_quantity; ?>
                                </span>
<!--                                 <div class="qty-plus-minus">
                                    <input class="qty-input" type="text" name="ec_qtybtn" value="1" />
                                </div> -->
                                <a href="javascript:void(0)" class="remove del-icon trash" data-productid="<?php echo $product_id; ?>">×</a>
                            </div>
                        </li>

                    <?php } ?>

                </ul>
            </div>
            <div class="ec-cart-bottom">
                <div class="cart-sub-total">
                    <table class="table cart-table">
                        <tbody>
    <!--                         <tr>
                                <td class="text-left">Sub-Total :</td>
                                <td class="text-right">$300.00</td>
                            </tr> -->
    <!--                         <tr>
                                <td class="text-left">VAT (20%) :</td>
                                <td class="text-right">$60.00</td>
                            </tr> -->
                            <tr>
                                <td class="text-left"><?php _e('Tổng', 'text_domain'); ?> :</td>
                                <td class="text-right primary-color"><?php echo $cart_total; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="cart_btn">
                    <a href="<?php echo $cart_page_url; ?>" class="btn btn-primary"><?php _e('Giỏ hàng', 'text_domain'); ?></a>
                    <a href="<?php echo $checkout_page_url; ?>" class="btn btn-secondary"><?php _e('Thanh toán', 'text_domain'); ?></a>
                </div>
            </div>

        <?php } else { ?>
            <?php _e('Giỏ hàng trống !', 'text_domain'); ?>
        <?php } ?>

    </div>
</div>