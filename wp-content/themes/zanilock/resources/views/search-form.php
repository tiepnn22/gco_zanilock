<div class="header-search">

	<form class="ec-btn-group-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	    <input type="text" class="form-control" required="required" placeholder="<?php _e('Tìm kiếm...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
	    <button type="submit" class="btn submit">
	    	<img src="<?php echo core_asset('images/icons/search.svg'); ?>" class="svg_img header_svg" alt="" />
	    </button>
	</form>
	
</div>