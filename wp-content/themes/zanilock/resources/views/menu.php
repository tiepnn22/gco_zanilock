<?php
    if(function_exists('wp_nav_menu')){
        $args = array(
            'theme_location' 	=> 	'primary',
            'container'         =>  '',         // bao ngoài
            'container_class'   =>  '',
            'container_id'      =>  '',
            'menu_class'		=>	'',        // class ul
            // 'items_wrap'        =>  '<ul id="menu" class="menu">%3$s</ul>'     // thay đổi html ul
        );
        wp_nav_menu( $args );
    }
?>