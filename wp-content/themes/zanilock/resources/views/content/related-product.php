<?php
	$post_id 			= get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= core_getPostImage($post_id,"p-product");
	$post_excerpt 		= core_cut_string(get_the_excerpt($post_id),300,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-6 pro-gl-content">
    <div class="ec-product-inner">
        <div class="ec-pro-image-outer">
            <div class="ec-pro-image">
                <a href="<?php echo $post_link; ?>" class="image">
                    <img class="main-image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
                </a>

                <?php
                	echo $core_show_sale = core_show_sale($post_id);
                	// if(!empty( $core_show_sale )) {
                	// 	echo '<span class="flags"><span class="sale">Sale</span></span>';
                	// }
            	?>

                <a href="javascript:void(0)" class="quickview" title="Quick view">
                    <?php echo do_shortcode('[woosq id="'.$post_id.'"]'); ?>
                </a>
                <div class="ec-pro-actions">
                    <div class="ec-btn-group compare" title="Compare">
                        <?php echo do_shortcode('[yith_compare_button]'); ?>
                    </div>
                    
                    <?php echo core_show_add_to_cart_button_ajax($post_id); ?>

                    <div class="ec-btn-group wishlist" title="Wishlist">
                        <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ec-pro-content">
            <h5 class="ec-pro-title">
            	<a href="<?php echo $post_link; ?>">
            		<?php echo $post_title; ?>
            	</a>
            </h5>
            <?php echo core_show_rating($post_id); ?>
            <div class="ec-pro-list-desc"><?php echo $post_excerpt; ?></div>
            <?php echo core_show_price_old_price($post_id); ?>
        </div>
    </div>
</div>