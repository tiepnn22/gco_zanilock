<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d-m-Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= core_getPostImage($post_id,"p-post");
	$post_excerpt 		= core_cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
    $post_comment       = wp_count_comments($post_id);
    $post_comment_total = $post_comment->total_comments;
    
	$get_category = get_the_category($post_id);
?>

<div class="col-lg-4 col-md-6 col-sm-12 mb-6 ec-blog-block">
    <div class="ec-blog-inner">
        <div class="ec-blog-image">
            <a href="<?php echo $post_link; ?>">
                <img class="blog-image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
            </a>
        </div>
        <div class="ec-blog-content">
            <h5 class="ec-blog-title">
            	<a href="<?php echo $post_link; ?>">
            		<?php echo $post_title; ?>
        		</a>
        	</h5>
            <div class="ec-blog-date">Bởi <span><?php echo $post_author; ?></span> / <?php echo $post_date; ?></div>
            <div class="ec-blog-desc"><?php echo $post_excerpt; ?></div>
            <div class="ec-blog-btn">
            	<a href="<?php echo $post_link; ?>" class="btn btn-primary">Xem thêm</a>
            </div>
        </div>
    </div>
</div>