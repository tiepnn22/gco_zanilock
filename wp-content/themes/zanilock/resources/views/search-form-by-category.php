<?php
    $terms = get_terms('product_cat', array(
        'parent'=> 0,
        'hide_empty' => false
    ) );
?>

<div class="header-search">
    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="ec-search-group-form">
        <div class="ec-search-select-inner">
            <select name="cate">
                <option>Tất cả</option>

				<?php
				    foreach ($terms as $foreach_kq) {
					    $post_id = $foreach_kq->term_id;
					    $post_title = $foreach_kq->name;
			    ?>
			    	<option <?php if($_GET['cate'] == $post_id) { echo 'selected'; }?> value="<?php echo $post_id; ?>">
			    		<?php echo $post_title; ?>
		    		</option>
			    <?php
					}
				?>

            </select>
        </div>
        <input type="text" class="form-control" required="required" placeholder="<?php _e('Tìm kiếm...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
        <button type="submit" class="search_submit">
            Tìm kiếm <img src="<?php echo core_asset('images/icons/search.svg'); ?>" class="svg_img search_svg" alt="" />
        </button>
    </form>
</div>