<?php
    $next_post     = get_next_post();
	$previous_post = get_previous_post();
?>

<div class="ec-blog-arrows">
    <?php
        if( !empty($previous_post) ) {
            $post_id    = $previous_post->ID;
            $post_title = get_the_title($post_id);
            $post_link  = get_permalink($post_id);
    ?>
        <a class="previous_post" href="<?php echo $post_link; ?>"><i class="ecicon eci-angle-left"></i> Bài trước</a>
    <?php } ?>

    <?php
        if( !empty($next_post) ) {
            $post_id    = $next_post->ID;
            $post_title = get_the_title($post_id);
            $post_link  = get_permalink($post_id);
    ?>
        <a class="next_post" href="<?php echo $post_link; ?>">Bài sau <i class="ecicon eci-angle-right"></i></a>
    <?php } ?>
</div>