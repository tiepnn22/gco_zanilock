<?php
    //field
    $f_socical_facebook     = get_field('f_socical_facebook', 'option');
    $f_socical_twiter       = get_field('f_socical_twiter', 'option');
    $f_socical_instagram    = get_field('f_socical_instagram', 'option');
    $f_socical_linkin       = get_field('f_socical_linkin', 'option');
?>

<ul class="mb-0">
    <li class="list-inline-item">
        <a class="<?php if( !is_front_page() ){ echo 'hdr-facebook'; } ?>" href="<?php echo $f_socical_facebook; ?>" target="_blank">
            <i class="ecicon eci-facebook"></i>
        </a>
    </li>
    <li class="list-inline-item">
        <a class="<?php if( !is_front_page() ){ echo 'hdr-twitter'; } ?>" href="<?php echo $f_socical_twiter; ?>" target="_blank">
            <i class="ecicon eci-twitter"></i>
        </a>
    </li>
    <li class="list-inline-item">
        <a class="<?php if( !is_front_page() ){ echo 'hdr-instagram'; } ?>" href="<?php echo $f_socical_instagram; ?>" target="_blank">
            <i class="ecicon eci-instagram"></i>
        </a>
    </li>
    <li class="list-inline-item">
        <a class="<?php if( !is_front_page() ){ echo 'hdr-linkedin'; } ?>" href="<?php echo $f_socical_linkin; ?>" target="_blank">
            <i class="ecicon eci-linkedin"></i>
        </a>
    </li>
</ul>