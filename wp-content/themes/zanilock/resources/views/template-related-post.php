<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in'         => $category_ids,
	    'post__not_in'         => array($post->ID),
	    'posts_per_page'       => -1,
	    'ignore_sticky_posts'  => 1
    );
    $query = new wp_query( $args );
?>


<section class="section related-post section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title">Tin tức liên quan</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="related-post-slider ec-trend-slider">

                <?php
                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                ?>

                    <?php get_template_part('resources/views/content/related-post', get_post_format()); ?>

                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>
        </div>
    </div>
</section>