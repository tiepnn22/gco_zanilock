<?php
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => 4,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>


<section class="section ec-releted-product section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <!-- <h2 class="ec-bg-title">Related products</h2> -->
                    <h2 class="ec-title"><?php _e('Sản phẩm liên quan', 'text_domain'); ?></h2>
                    <!-- <p class="sub-title">Browse The Collection of Top Products</p> -->
                </div>
            </div>
        </div>
        <div class="row margin-minus-b-30">

			<?php
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
			?>

				<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

			<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</section>