<?php
// Register Style
function core_theme_Style() {
    // css
    wp_register_style( 'ecicons-style', get_stylesheet_directory_uri() . "/dist/css/vendor/ecicons.min.css",false, 'all' );
    wp_enqueue_style('ecicons-style');
    // wp_register_style( 'animate-style', get_stylesheet_directory_uri() . "/dist/css/plugins/animate.css",false, 'all' );
    // wp_enqueue_style('animate-style');
    // wp_register_style( 'swiper-bundle-style', get_stylesheet_directory_uri() . "/dist/css/plugins/swiper-bundle.min.css",false, 'all' );
    // wp_enqueue_style('swiper-bundle-style');
    wp_register_style( 'jquery-ui-style', get_stylesheet_directory_uri() . "/dist/css/plugins/jquery-ui.min.css",false, 'all' );
    wp_enqueue_style('jquery-ui-style');
    // wp_register_style( 'countdownTimer-style', get_stylesheet_directory_uri() . "/dist/css/plugins/countdownTimer.css",false, 'all' );
    // wp_enqueue_style('countdownTimer-style');
    wp_register_style( 'slick-style', get_stylesheet_directory_uri() . "/dist/css/plugins/slick.min.css",false, 'all' );
    wp_enqueue_style('slick-style');
    // wp_register_style( 'nouislider-style', get_stylesheet_directory_uri() . "/dist/css/plugins/nouislider.css",false, 'all' );
    // wp_enqueue_style('nouislider-style');
    wp_register_style( 'bootstrap-style', get_stylesheet_directory_uri() . "/dist/css/plugins/bootstrap.css",false, 'all' );
    wp_enqueue_style('bootstrap-style');

    if( !is_front_page() ){
        wp_register_style( 'main-style', get_stylesheet_directory_uri() . "/dist/css/style.css",false, 'all' );
        wp_enqueue_style('main-style');
        wp_register_style( 'responsive-style', get_stylesheet_directory_uri() . "/dist/css/responsive.css",false, 'all' );
        wp_enqueue_style('responsive-style');
    } else {
        wp_register_style( 'main-home-style', get_stylesheet_directory_uri() . "/dist/css/demo3.css",false, 'all' );
        wp_enqueue_style('main-home-style');
    }

    wp_register_style( 'wc-style', get_stylesheet_directory_uri() . "/dist/css/wc.css",false, 'all' );
    wp_enqueue_style('wc-style');
    wp_register_style( 'wp-style', get_stylesheet_directory_uri() . "/dist/css/wp.css",false, 'all' );
    wp_enqueue_style('wp-style');


    // js
    wp_register_script( 'jquery-script', get_stylesheet_directory_uri() . "/dist/js/vendor/jquery-3.5.1.min.js", array('jquery'),false,false );
    wp_enqueue_script('jquery-script');
    wp_register_script( 'popper-script', get_stylesheet_directory_uri() . "/dist/js/vendor/popper.min.js", array('jquery'),false,true );
    wp_enqueue_script('popper-script');
    wp_register_script( 'bootstrap-script', get_stylesheet_directory_uri() . "/dist/js/vendor/bootstrap.min.js", array('jquery'),false,true );
    wp_enqueue_script('bootstrap-script');
    wp_register_script( 'jquery-migrate-script', get_stylesheet_directory_uri() . "/dist/js/vendor/jquery-migrate-3.3.0.min.js", array('jquery'),false,true );
    wp_enqueue_script('jquery-migrate-script');
    wp_register_script( 'modernizr-script', get_stylesheet_directory_uri() . "/dist/js/vendor/modernizr-3.11.2.min.js", array('jquery'),false,true );
    wp_enqueue_script('modernizr-script');
    // wp_register_script( 'swiper-bundle-script', get_stylesheet_directory_uri() . "/dist/js/plugins/swiper-bundle.min.js", array('jquery'),false,true );
    // wp_enqueue_script('swiper-bundle-script');

    if( is_front_page() ){
        wp_register_script( 'countdownTimer-script', get_stylesheet_directory_uri() . "/dist/js/plugins/countdownTimer.min.js", array('jquery'),false,true );
        wp_enqueue_script('countdownTimer-script');
    }

    // wp_register_script( 'scrollup-script', get_stylesheet_directory_uri() . "/dist/js/plugins/scrollup.js", array('jquery'),false,true );
    // wp_enqueue_script('scrollup-script');
    // wp_register_script( 'zoom-script', get_stylesheet_directory_uri() . "/dist/js/plugins/jquery.zoom.min.js", array('jquery'),false,true );
    // wp_enqueue_script('zoom-script');
    wp_register_script( 'slick-script', get_stylesheet_directory_uri() . "/dist/js/plugins/slick.min.js", array('jquery'),false,true );
    wp_enqueue_script('slick-script');

    if( is_front_page() ){
        wp_register_script( 'infiniteslidev2-script', get_stylesheet_directory_uri() . "/dist/js/plugins/infiniteslidev2.js", array('jquery'),false,true );
        wp_enqueue_script('infiniteslidev2-script');
    }
    
    // wp_register_script( 'magnific-popup-script', get_stylesheet_directory_uri() . "/dist/js/vendor/jquery.magnific-popup.min.js", array('jquery'),false,true );
    // wp_enqueue_script('magnific-popup-script');
    // wp_register_script( 'sticky-sidebar-script', get_stylesheet_directory_uri() . "/dist/js/plugins/jquery.sticky-sidebar.js", array('jquery'),false,true );
    // wp_enqueue_script('sticky-sidebar-script');
    // wp_register_script( 'index-script', get_stylesheet_directory_uri() . "/dist/js/vendor/index.js", array('jquery'),false,true );
    // wp_enqueue_script('index-script');

    if( !is_front_page() ){
        wp_register_script( 'main-script', get_stylesheet_directory_uri() . "/dist/js/main.js", array('jquery'),false,true );
        wp_enqueue_script('main-script');
    } else {
        wp_register_script( 'main-home-script', get_stylesheet_directory_uri() . "/dist/js/demo-3.js", array('jquery'),false,true );
        wp_enqueue_script('main-home-script');
    }
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'core_theme_Style');


// Register Menu and image
if (!function_exists('core_theme_Setup')) {
    function core_theme_Setup()
    {
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'text_domain' ),
            'f-info' => __( 'Menu footer thông tin', 'text_domain' ),
            'f-account' => __( 'Menu footer tài khoản', 'text_domain' ),
            'f-services' => __( 'Menu footer dịch vụ', 'text_domain' ),
            'h-category' => __( 'Menu header danh mục', 'text_domain' ),
        ) );

        add_theme_support( 'woocommerce' );
	    add_theme_support( 'post-thumbnails' );
        add_image_size( 'p-post', 360, 200, true );
	    add_image_size( 'p-product', 260, 290, true );
    }
    add_action('after_setup_theme', 'core_theme_Setup');
}


// Register Sidebar
if (!function_exists('core_theme_Widgets')) {
    function core_theme_Widgets()
    {
        $sidebars = [
    //         [
				// 'name'          => __( 'Sidebar tin tức', 'text_domain' ),
				// 'id'            => 'sidebar-news',
				// 'description'   => __( 'Vùng sidebar trang danh mục + chi tiết', 'text_domain' ),
				// 'before_widget' => '<div id="%1$s" class="widget %2$s">',
				// 'after_widget'  => '</div>',
				// 'before_title'  => '<h3 class="widget-title">',
				// 'after_title'   => '</h3>',
    //         ],
            [
                'name'          => __( 'Sidebar sản phẩm', 'text_domain' ),
                'id'            => 'sidebar-product',
                'description'   => __( 'Vùng sidebar trang danh mục + chi tiết ', 'text_domain' ),
                'before_sidebar' => '<div class="ec-sidebar-block">',
                'after_sidebar'  => '</div>',
                'before_widget' => '<div id="%1$s" class="widget %2$s ec-sb-block-content">',
                'after_widget'  => '</div>',
                'before_title'  => '<div class="ec-sb-title"><h3 class="ec-sidebar-title">',
                'after_title'   => '</h3></div>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'core_theme_Widgets');
}


// Shortcode play on Widget
// add_filter('widget_text','do_shortcode');

// Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
// Disables the block editor from managing widgets.
add_filter( 'use_widgets_block_editor', '__return_false' );
// Use Block Editor default for Post
add_filter('use_block_editor_for_post', '__return_false');


// Delete class and id of wp_nav_menu()
function core_wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'core_wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'core_wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'core_wp_nav_menu_attributes_filter', 100, 1);


// Delete span, br on Contact Form 7
add_filter('wpcf7_form_elements', function($content) {
    // Delete span (ko xoá đc vì sẽ mất thông báo validate)
    // $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    // Delete br
    $content = str_replace('<br />', '', $content);
    return $content;
});

// Remove Contact Form 7 script and css
// add_filter( 'wpcf7_load_js', '__return_false' );
// add_filter( 'wpcf7_load_css', '__return_false' );

// Call funtion Contact Form 7 script and css
// if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
// if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }

// Shortcode Call funtion Contact Form 7 script and css
// function shortcode_call_script_css_cf7($args, $content) {
//     if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
//     if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
// }
// add_shortcode( 'shortcode_get_script_css_cf7', 'shortcode_call_script_css_cf7' );
// echo do_shortcode('[shortcode_get_script_css_cf7]');


// Setup SMTP
function core_setup_smtp_email( $phpmailer ) {
    $phpmailer->isSMTP();     
    $phpmailer->Host        = 'smtp.gmail.com';
    $phpmailer->Port        = get_field('smtp_port', 'option');
    $phpmailer->SMTPAuth    = true;
    $phpmailer->Username    = get_field('smtp_user', 'option');
    $phpmailer->Password    = get_field('smtp_pass', 'option');
    $phpmailer->SMTPSecure  = get_field('smtp_encryption', 'option');
}
add_action( 'phpmailer_init', 'core_setup_smtp_email' );


// Remove admin menu
function core_custom_admin_menu() {
    // remove
    remove_menu_page( 'index.php' );                             // index
    // remove_menu_page( 'edit-comments.php' );                     // comments
    remove_menu_page( 'tools.php' );                             // tools
    // remove_menu_page( 'edit.php?post_type=acf-field-group' );    // acf field
    remove_menu_page( 'wpclever' );                              // woo quick-view
    // remove_menu_page( 'woocommerce' );                           // woo chính (đơn hàng, cài đặt, ...)
    remove_menu_page( 'woocommerce-marketing' );                 // woo marketing (tiếp thị)

    // remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );   // tag post
    remove_submenu_page( 'themes.php', 'theme-editor.php' );                // themes
    remove_submenu_page( 'plugins.php', 'plugin-editor.php' );              // plugins
    remove_submenu_page( 'options-general.php', 'options-writing.php' );    // setting
    remove_submenu_page( 'options-general.php', 'options-discussion.php' );
    remove_submenu_page( 'options-general.php', 'options-media.php' );
    remove_submenu_page( 'options-general.php', 'options-privacy.php' );
    remove_submenu_page( 'wpcf7', 'wpcf7-integration' );                    // cf7
    remove_submenu_page( 'contact-form-listing', 'import_cf7_csv' );        // cf7 db
    remove_submenu_page( 'contact-form-listing', 'shortcode' );
    remove_submenu_page( 'contact-form-listing', 'extentions' );
    remove_submenu_page( 'contact-form-listing', 'mounstride-CRM' );
    remove_submenu_page( 'plugins.php', 'remove_taxonomy_base_slug' );      // remove_taxonomy_base_slug
    remove_submenu_page( 'options-general.php', 'breadcrumb-navxt' );       // breadcrumb-navxt

    global $menu;       // Global to get menu array
    global $submenu;    // Global to get submenu array

    // rename
    $menu[10][0] = 'Thư viện ảnh';                      // gallery
    if( $menu[45][0] == 'Advanced CF7 DB') {
        $menu[45][0] = 'Dữ liệu form';                  // cf7 db
    }
    $submenu['themes.php'][5][0] = 'Giao diện';         // submenu theme

    // remove
    $submenu['themes.php'][6][1] = '';                  // submenu customize
    // $submenu['edit.php?post_type=product'][16][1] = ''; // woo tag
    // $submenu['edit.php?post_type=product'][17][1] = ''; // woo attribute
}
add_action( 'admin_menu', 'core_custom_admin_menu' );




// Query_post_by_custompost
function core_query_post_by_custompost($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     => $posttype_name,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function core_query_post_by_custompost_paged($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     => $posttype_name,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_category
function core_query_post_by_category($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat'           => $cat_id,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function core_query_post_by_category_paged($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat'           => $cat_id,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_tag
function core_query_post_by_tag($tag_id, $numPost){
    $qr =  new WP_Query( array(
                        'tag_id'        => $tag_id,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function core_query_post_by_tag_paged($tag_id, $numPost){
    $qr =  new WP_Query( array(
                        'tag_id'        => $tag_id,
                        'post_status'   => array('publish'),
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_taxonomy
function core_query_post_by_taxonomy($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     =>  $posttype_name,
                        'tax_query'     =>  array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'post_status'   => array('publish'),
                        'showposts'     =>  $numPost,
                        'order'         =>  'DESC',
                        'orderby'       =>  'date'
                ) );
    return $qr;
}
function core_query_post_by_taxonomy_paged($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     =>  $posttype_name,
                        'tax_query'     =>  array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'post_status'   => array('publish'),
                        'showposts'     =>  $numPost,
                        'order'         =>  'DESC',
                        'orderby'       =>  'date',
                        'paged'         =>  (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_only_taxonomy ($taxonomy_name là loại trù taxonomy_name đó ra)
// function core_query_post_by_only_taxonomy($posttype_name, $taxonomy_name, $numPost){
//     $qr =  new WP_Query( array(
//                         'post_type'     =>  $posttype_name,
//                         'tax_query'     =>  array(
//                                                 array(
//                                                         'taxonomy'  => $taxonomy_name,
//                                                         'operator' => 'NOT EXISTS'
//                                                 )),
//                         'post_status'   => array('publish'),
//                         'showposts'     =>  $numPost,
//                         'order'         =>  'DESC',
//                         'orderby'       =>  'date'
//                 ) );
//     return $qr;
// }
// function core_query_post_by_only_taxonomy_paged($posttype_name, $taxonomy_name, $numPost){
//     $qr =  new WP_Query( array(
//                         'post_type'     =>  $posttype_name,
//                         'tax_query'     =>  array(
//                                                 array(
//                                                         'taxonomy'  => $taxonomy_name,
//                                                         'operator' => 'NOT EXISTS'
//                                                 )),
//                         'post_status'   => array('publish'),
//                         'showposts'     =>  $numPost,
//                         'order'         =>  'DESC',
//                         'orderby'       =>  'date',
//                         'paged'         =>  (get_query_var('paged')) ? get_query_var('paged') : 1
//                 ) );
//     return $qr;
// }


// Query_page
// function core_query_page(){
//     $qr =  new WP_Query( array(
//                         'post_type'      => 'page',
//                         'post_status'    => array('publish'),
//                         'showposts'      => -1,
//                         'order'          => 'DESC',
//                         'orderby'        => 'menu_order'
//                 ) );
//     return $qr;
// }


// Query_page_by_page_parent
// function core_query_page_by_page_parent($page_id){
//     $qr =  new WP_Query( array(
//                         'post_type'      => 'page',
//                         'post_parent'    => $page_id,
//                         'post_status'    => array('publish'),
//                         'showposts'      => -1,
//                         'order'          => 'DESC',
//                         'orderby'        => 'menu_order'
//                 ) );
//     return $qr;
// }


// Query_search_post
function core_query_search_post($keyword, $posttype_array, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'post_type'      => $posttype_array,
                        'post_status'    => array('publish'),
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type'
                ) );
    return $qr;
}
function core_query_search_post_paged($keyword, $posttype_array, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'post_type'      => $posttype_array,
                        'post_status'    => array('publish'),
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type',
                        'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_search_post_by_category
// function core_query_search_post_by_category($keyword, $cat_id, $posttype_name, $numPost){
//     $qr =  new WP_Query( array(
//                         's'              => $keyword,
//                         'cat'            => $cat_id,
//                         'post_type'      => $posttype_name,
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'type'
//                 ) );
//     return $qr;
// }
// function core_query_search_post_by_category_paged($keyword, $cat_id, $posttype_name, $numPost){
//     $qr =  new WP_Query( array(
//                         's'              => $keyword,
//                         'cat'            => $cat_id,
//                         'post_type'      => $posttype_name,
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'type',
//                         'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
//                 ) );
//     return $qr;
// }


// Query_search_post_by_taxonomy
function core_query_search_post_by_taxonomy($keyword, $term_id, $posttype_name, $taxonomy_name, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'tax_query'      => array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'post_type'      => $posttype_array,
                        'post_status'    => array('publish'),
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type'
                ) );
    return $qr;
}
function core_query_search_post_by_taxonomy_paged($keyword, $term_id, $posttype_name, $taxonomy_name, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'tax_query'      => array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'post_type'      => $posttype_array,
                        'post_status'    => array('publish'),
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type',
                        'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_views_hot
// function core_query_post_views_hot($numPost){
//     $qr =  new WP_Query( array(
//                         'meta_key'       => 'post_views_count',
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'meta_value_num'
//                 ) );
//     return $qr;
// }
// function core_query_post_views_hot_paged($numPost){
//     $qr =  new WP_Query( array(
//                         'meta_key'       => 'post_views_count',
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'meta_value_num',
//                         'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
//                 ) );
//     return $qr;
// }


// Query_post_buy_hot
// function core_query_post_buy_hot($posttype_name, $numPost){
//     $qr =  new WP_Query( array(
//                         'post_type'      => $posttype_name,
//                         'meta_key'       => 'total_sales',
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'meta_value_num'
//                 ) );
//     return $qr;
// }
// function core_query_post_buy_hot_paged($posttype_name, $numPost){
//     $qr =  new WP_Query( array(
//                         'post_type'      => $posttype_name,
//                         'meta_key'       => 'total_sales',
//                         'post_status'    => array('publish'),
//                         'showposts'      => $numPost,
//                         'order'          => 'DESC',
//                         'orderby'        => 'meta_value_num',
//                         'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
//                 ) );
//     return $qr;
// }



// Edit Footer on Admin
if (!function_exists('core_remove_footer_admin')) {
    function core_remove_footer_admin () {
        echo 'Thiết kế website bởi <a href="http://gco.vn/" target="_blank">GCO</a>';
    }
    add_filter('admin_footer_text', 'core_remove_footer_admin');
}


// Close all Update
if (!function_exists('core_remove_core_updates')) {
    function core_remove_core_updates() {
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','core_remove_core_updates');
    add_filter('pre_site_transient_update_plugins','core_remove_core_updates');
    add_filter('pre_site_transient_update_themes','core_remove_core_updates');
}


// Custom Favicon
if (!function_exists('core_custom_favicon')) {
    function core_custom_favicon() {
        echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_field('favicon', 'option').'" />';
    }
    add_action('wp_head', 'core_custom_favicon');
}


// Custom logo admin bar
if (!function_exists('core_wpb_custom_logo')) {
    function core_wpb_custom_logo() {
        echo '
            <style type="text/css">
                #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon::before {
                    background-image: url('.get_field('favicon', 'option').');
                    background-position: left center;
                    background-repeat: no-repeat;
                    background-size: contain;
                    -moz-background-size: contain;
                    -webkit-background-size: contain;
                    -o-background-size: contain;
                    -ms-background-size: contain;
                    content: "";
                    display: inline-block;
                    height: 20px;
                    left: 3px;
                    top: 3px;
                    width: 25px;
                }
            </style>';
    }
    add_action('wp_before_admin_bar_render', 'core_wpb_custom_logo');
}


// Custom logo admin login
if (!function_exists('core_my_login_logo_one')) {
    function core_my_login_logo_one() {
        echo '
            <style type="text/css">
                body.login div#login h1 a {
                    background-image: url('.get_field('favicon', 'option').');
                    background-position: center center;
                    background-repeat: no-repeat;
                    background-size: contain;
                    -moz-background-size: contain;
                    -webkit-background-size: contain;
                    -o-background-size: contain;
                    -ms-background-size: contain;
                    display: inline-block;
                    height: 200px;
                    line-height: 0;
                    margin: -30px auto 0;
                    width: 200px;
                }
            </style>';
    }
    add_action( 'login_enqueue_scripts', 'core_my_login_logo_one' );
}

?>