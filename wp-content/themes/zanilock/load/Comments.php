<?php
// Tuỳ biến form mặc định
function gb_comment_form_tweaks ($fields) {
    //add placeholders and remove labels
    $fields['author'] = '<input id="author" name="author" value="" placeholder="Tên của bạn..." size="30" maxlength="245" required="required" type="text">';
    $fields['email'] = '<input id="email" name="email" type="email" value="" placeholder="Email..." size="30" maxlength="100" aria-describedby="email-notes" required="required">';   

    //unset comment so we can recreate it at the bottom
    // unset($fields['comment']);

    $fields['comment'] = '<textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" placeholder="Đăng bình luận của bạn tại... " required="required"></textarea>';

    //remove website
    unset($fields['url']);

    return $fields;
}
add_filter('comment_form_fields', 'gb_comment_form_tweaks');