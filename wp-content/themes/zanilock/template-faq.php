<?php
	/*
	Template Name: Mẫu Hỏi đáp
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );

    //field
    $faq_faq_section_title	= get_field('faq_faq_section_title');
    $faq_faq_section_desc	= get_field('faq_faq_section_desc');

    $faq_faq_title		= get_field('faq_faq_title');
    $faq_faq_content    = get_field('faq_faq_content');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $faq_faq_section_title; ?></h2>
                    <p class="sub-title mb-3"><?php echo $faq_faq_section_desc; ?></p>
                </div>
            </div>
            <div class="ec-faq-wrapper">
                <div class="ec-faq-container">
                    <h2 class="ec-faq-heading"><?php echo $faq_faq_title; ?></h2>

                    <?php if(!empty( $faq_faq_content )) { ?>
                    <div id="ec-faq">
						
						<?php
						    foreach ($faq_faq_content as $foreach_kq) {

						    $post_faq = $foreach_kq["faq"];
						    $post_rep = $foreach_kq["rep"];
						?>
	                        <div class="col-sm-12 ec-faq-block">
	                            <h4 class="ec-faq-title"><?php echo $post_faq; ?></h4>
	                            <div class="ec-faq-content">
	                                <p><?php echo $post_rep; ?></p>
	                            </div>
	                        </div>
						<?php } ?>
						
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>