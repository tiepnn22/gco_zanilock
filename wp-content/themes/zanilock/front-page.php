<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
	//field
	$popup_form_title 	= get_field('popup_form_title');
	$popup_form_desc 	= get_field('popup_form_desc');
    $popup_form_id      = get_field('popup_form');
    $popup_form         = do_shortcode('[contact-form-7 id="'.$popup_form_id.'"]');

    $home_slide = get_field('home_slide');

    $home_product_title 		= get_field('home_product_title');
    $home_product_desc			= get_field('home_product_desc');
    $home_product_select_cat 	= get_field('home_product_select_cat');
    $home_product_url 			= get_field('home_product_url');
    $home_product_button_text 	= get_field('home_product_button_text');

    $home_box_banner_content = get_field('home_box_banner_content');

    $home_product_tab_title         = get_field('home_product_tab_title');
    $home_product_tab_desc          = get_field('home_product_tab_desc');
    $home_product_tab_select_cat    = get_field('home_product_tab_select_cat');

    $home_sale_image 		= get_field('home_sale_image');
    $home_sale_title 		= get_field('home_sale_title');
    $home_sale_sale 		= get_field('home_sale_sale');
    $home_sale_desc 		= get_field('home_sale_desc');
    $home_sale_url 			= get_field('home_sale_url');
    $home_sale_button_text 	= get_field('home_sale_button_text');
    $home_sale_date 		= get_field('home_sale_date');
    $home_date_here = date('Y-m-d H:i:s');

    $home_product_feature_title 		= get_field('home_product_feature_title');
    $home_product_feature_desc 			= get_field('home_product_feature_desc');
    $home_product_feature_select 		= get_field('home_product_feature_select');
    $home_product_feature_url 			= get_field('home_product_feature_url');
    $home_product_feature_button_text 	= get_field('home_product_feature_button_text');

    $home_service_content = get_field('home_service_content');

    $home_testimonial_title 	= get_field('home_testimonial_title');
    $home_testimonial_desc 		= get_field('home_testimonial_desc');
    $home_testimonial_content 	= get_field('home_testimonial_content');

    $home_instagram_title 			= get_field('home_instagram_title');
    $home_instagram_desc 			= get_field('home_instagram_desc');
    $home_instagram_content 		= get_field('home_instagram_content');
    $home_instagram_url 			= get_field('home_instagram_url');
    $home_instagram_button_text 	= get_field('home_instagram_button_text');

    $home_news_title           = get_field('home_news_title');
    $home_news_desc            = get_field('home_news_desc');
    $home_news_select_post     = get_field('home_news_select_post');
    $home_news_url             = get_field('home_news_url');
    $home_news_button_text     = get_field('home_news_button_text');

    $home_partner_content = get_field('home_partner_content');
?>

<!-- Newsletter Modal Start -->
<div id="ec-popnews-bg"></div>
<div id="ec-popnews-box">
    <div id="ec-popnews-close"><i class="ecicon eci-close"></i></div>
    <div id="ec-popnews-box-content">
        <h1><?php echo $popup_form_title; ?></h1>
        <p><?php echo $popup_form_desc; ?></p>

        <?php if(!empty( $popup_form )) { ?>
        <div  id="ec-popnews-form">
            <?php echo $popup_form; ?>
        </div>
        <?php } ?>
    </div>
</div>

<?php if(!empty( $home_slide )) { ?>
<div class="ec-main-slider section section-space-mb">
    <div class="ec-slider">

		<?php
		    foreach ($home_slide as $foreach_kq) {

		    $post_image 		= $foreach_kq["image"];
		    $post_title 		= $foreach_kq["title"];
		    $post_title_small 	= $foreach_kq["title_small"];
		    $post_desc 			= $foreach_kq["desc"];
		    $post_desc_small 	= $foreach_kq["desc_small"];
		    $post_button_text 	= $foreach_kq["button_text"];
		    $post_url 			= $foreach_kq["url"];
		?>
	        <div class="ec-slide-item d-flex slide-item" style="background-image: url(<?php echo $post_image; ?>);" >
	            <img src="<?php echo $post_image; ?>" class="main_banner_arrow_img" alt="" />
	            <div class="container align-self-center">
	                <div class="row">
	                    <div class="col-xl-6 col-lg-7 col-md-7 col-sm-7 align-self-center">
	                        <div class="ec-slide-content slider-animation">
	                            <h2 class="ec-slide-stitle"><?php echo $post_title; ?></h2>
	                            <h1 class="ec-slide-title"><?php echo $post_title_small; ?></h1>
	                            <p><?php echo $post_desc; ?></p>
	                            <span class="ec-slide-disc"><?php echo $post_desc_small; ?></span>
	                            <a href="<?php echo $post_url; ?>" class="btn btn-lg btn-secondary"><?php echo $post_button_text; ?></a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
		<?php } ?>

    </div>
</div>
<?php } ?>

<section class="section ec-category-section section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_product_title; ?></h2>
                    <p class="sub-title"><?php echo $home_product_desc; ?></p>
                </div>
                <div class="section-btn">
                    <span class="ec-section-btn">
                    	<a class="btn-primary" href="<?php echo $home_product_url; ?>">
                    		<?php echo $home_product_button_text; ?>
                		</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
        	<?php if(!empty( $home_product_select_cat )) { ?>
            <div class="ec_cat_slider">

				<?php
					foreach ($home_product_select_cat as $foreach_kq) {

					$term_id 		= $foreach_kq->term_id;
					$taxonomy_slug 	= $foreach_kq->taxonomy;
					// $term_desc 		= core_cut_string( $foreach_kq->description,300,'...' );
					$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
					$term_link 		= esc_url( get_term_link( get_term($term_id) ) );
					$thumbnail_id 		= get_term_meta( $term_id, 'thumbnail_id', true ); // woo
				    $term_image_check	= wp_get_attachment_url( $thumbnail_id ); // woo
				    $term_image			= (!empty($term_image_check)) ? $term_image_check : ''; // woo
				?>
	                <div class="ec_cat_content">
	                    <div class="ec_cat_inner">
	                        <div class="ec-cat-image">
	                            <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>" />
	                        </div>
	                        <div class="ec-cat-desc">
	                            <span class="ec-section-btn">
	                            	<a href="<?php echo $term_link; ?>" class="btn-primary">
	                            		<?php echo $term_name; ?>
	                            	</a>
	                            </span>
	                        </div>
	                    </div>
	                </div>
				<?php } ?>

            </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php if(!empty( $home_box_banner_content )) { ?>
<section class="ec-banner section section-space-p">
    <div class="ec-banners">
        <div class="ec-banner-left col-sm-6">
            <div class="ec-banner-block ec-banner-block-1 col-sm-12">
                <div class="banner-block">
                    <img src="<?php echo $home_box_banner_content[0]["image"]; ?>" alt="" />
                    <div class="banner-content">
                        <div class="banner-text">
                            <span class="ec-banner-stitle"><?php echo $home_box_banner_content[0]["title_small"]; ?></span>
                            <span class="ec-banner-title"><?php echo $home_box_banner_content[0]["title"]; ?></span>

                            <?php if(!empty( $home_box_banner_content[0]["desc"] )) { ?>
                            <p><?php echo $home_box_banner_content[0]["desc"]; ?></p>
                            <?php } ?>
                        </div>

                        <?php if(!empty( $home_box_banner_content[0]["button_text"] )) { ?>
                        <span class="ec-banner-btn">
                            <a href="<?php echo $home_box_banner_content[0]["url"]; ?>" class="btn-primary">
                                <?php echo $home_box_banner_content[0]["button_text"]; ?>
                            </a>
                        </span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ec-banner-right col-sm-6">
            <div class="ec-banner-block ec-banner-block-2 col-sm-12">
                <div class="banner-block">
                    <img src="<?php echo $home_box_banner_content[1]["image"]; ?>" alt="" />
                    <div class="banner-content">
                        <div class="banner-text">
                            <span class="ec-banner-stitle"><?php echo $home_box_banner_content[1]["title_small"]; ?></span>
                            <span class="ec-banner-title"><?php echo $home_box_banner_content[1]["title"]; ?></span>

                            <?php if(!empty( $home_box_banner_content[1]["desc"] )) { ?>
                            <p><?php echo $home_box_banner_content[1]["desc"]; ?></p>
                            <?php } ?>
                        </div>

                        <?php if(!empty( $home_box_banner_content[1]["button_text"] )) { ?>
                        <span class="ec-banner-btn">
                            <a href="<?php echo $home_box_banner_content[1]["url"]; ?>" class="btn-primary">
                                <?php echo $home_box_banner_content[1]["button_text"]; ?>
                            </a>
                        </span>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="ec-banner-block ec-banner-block-3 col-sm-6">
                <div class="banner-block2">
                    <div class="banner-block">
                        <img src="<?php echo $home_box_banner_content[2]["image"]; ?>" alt="" />
                        <div class="banner-content">
                            <div class="banner-text">
                                <span class="ec-banner-stitle"><?php echo $home_box_banner_content[2]["title_small"]; ?></span>
                                <span class="ec-banner-title"><?php echo $home_box_banner_content[2]["title"]; ?></span>

                                <?php if(!empty( $home_box_banner_content[2]["desc"] )) { ?>
                                    <span class="ec-banner-desc"><?php echo $home_box_banner_content[2]["desc"]; ?></span>
                                <?php } ?>
                            </div>

                            <?php if(!empty( $home_box_banner_content[2]["button_text"] )) { ?>
                            <span class="ec-banner-btn">
                                <a href="<?php echo $home_box_banner_content[2]["url"]; ?>" class="btn-primary">
                                    <?php echo $home_box_banner_content[2]["button_text"]; ?>
                                </a>
                            </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ec-banner-block ec-banner-block-4 col-sm-6">
                <div class="banner-block2">
                    <div class="banner-block">
                        <img src="<?php echo $home_box_banner_content[3]["image"]; ?>" alt="" />
                        <div class="banner-content">
                            <div class="banner-text">
                                <span class="ec-banner-stitle"><?php echo $home_box_banner_content[3]["title_small"]; ?></span>
                                <span class="ec-banner-title"><?php echo $home_box_banner_content[3]["title"]; ?></span>

                                <?php if(!empty( $home_box_banner_content[3]["desc"] )) { ?>
                                    <span class="ec-banner-desc"><?php echo $home_box_banner_content[3]["desc"]; ?></span>
                                <?php } ?>
                            </div>

                            <?php if(!empty( $home_box_banner_content[3]["button_text"] )) { ?>
                            <span class="ec-banner-btn">
                                <a href="<?php echo $home_box_banner_content[3]["url"]; ?>" class="btn-primary">
                                    <?php echo $home_box_banner_content[3]["button_text"]; ?>
                                </a>
                            </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="section ec-product-tab section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_product_tab_title; ?></h2>
                    <p class="sub-title"><?php echo $home_product_tab_desc; ?></p>
                </div>

                <?php if(!empty( $home_product_tab_select_cat )) { ?>
                <div class="section-btn">
                    <ul class="ec-pro-tab-nav nav">

                        <?php
                            $i=1;
                            foreach ($home_product_tab_select_cat as $foreach_kq) {

                            $term_id        = $foreach_kq->term_id;
                            $taxonomy_slug  = $foreach_kq->taxonomy;
                            $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                        ?>
                            <li class="nav-item">
                                <a class="nav-link <?php if($i == 1){ echo 'active'; } ?>" data-bs-toggle="tab" href="#item_<?php echo $i;?>">
                                    <?php echo $term_name; ?>
                                </a>
                            </li>
                        <?php $i++; } ?>

                    </ul>
                </div>
                <?php } ?>
            </div>
        </div>

        <?php if(!empty( $home_product_tab_select_cat )) { ?>
        <div class="row">
            <div class="col">
                <div class="tab-content">

                    <?php
                        $i=1;
                        foreach ($home_product_tab_select_cat as $foreach_kq) {
                        
                        $term_id        = $foreach_kq->term_id;
                        $taxonomy_slug  = $foreach_kq->taxonomy;
                        $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                    ?>

                        <div class="tab-pane fade show <?php if($i == 1){ echo 'active'; } ?>" id="item_<?php echo $i;?>">
                            <div class="row">
                                <div class="ec-pro-tab-slider row">

                                    <?php
                                        $query = core_query_post_by_taxonomy('product', $taxonomy_slug, $term_id, 8);
                                        if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                    ?>
                                        
                                        <?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

                                    <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                </div>
                            </div>
                        </div>

                    <?php $i++; } ?>

                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</section>

<section class="section ec-offer-section section-space-mt section-space-mb" style="background-image: url(<?php echo $home_sale_image; ?>);">
    <div class="container">
        <div class="row">
            <div class="ec-offer-inner">
                <div class="col-sm-4 ec-offer-content">
                    <h2 class="ec-offer-stitle"><?php echo $home_sale_title; ?></h2>
                    <h2 class="ec-offer-title"><?php echo $home_sale_sale; ?></h2>
                    <span class="ec-offer-desc"><?php echo $home_sale_desc; ?></span>
                    <div class="countdowntimer"><span id="ec-offer-count"></span></div>
                    <span class="ec-offer-btn">
                    	<a href="<?php echo $home_sale_url; ?>" class="btn btn-lg btn-secondary">
                    		<?php echo $home_sale_button_text; ?>
                		</a>
                	</span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section ec-trend-product section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_product_feature_title; ?></h2>
                    <p class="sub-title"><?php echo $home_product_feature_desc; ?></p>
                </div>
                <div class="section-btn">
                    <span class="ec-section-btn">
                    	<a class="btn-secondary" href="<?php echo $home_product_feature_url; ?>">
                    		<?php echo $home_product_feature_button_text; ?>
                    	</a>
                	</span>
                </div>
            </div>
        </div>
        <div class="row">

        	<?php if(!empty( $home_product_feature_select )) { ?>
            <div class="ec-trend-slider">

				<?php
				    foreach ($home_product_feature_select as $foreach_kq) {

				    $post_id 			= $foreach_kq->ID;
				    $post_title 		= get_the_title($post_id);
				    $post_link 			= get_permalink($post_id);
				    $post_image 		= core_getPostImage($post_id,"p-product");
				?>
	                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 ec-product-content">
	                    <div class="ec-product-inner">
	                        <div class="ec-pro-image-outer">
	                            <div class="ec-pro-image">
					                <a href="<?php echo $post_link; ?>" class="image">
					                    <img class="main-image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" />
					                </a>
                                    <span class="flags">
                                        <?php echo $core_show_sale = core_show_sale($post_id); ?>
                                    </span>
                                    <div class="ec-pro-actions">
                                        <?php echo core_show_add_to_cart_button_ajax($post_id); ?>
                                        <div class="ec-btn-group compare" title="Compare">
                                            <?php
                                                echo do_shortcode( '[yith_compare_button product="'.$post_id.'"]' );
                                            ?>
                                        </div>
                                        <a href="javascript:void(0)" class="ec-btn-group quickview" title="Quick view">
                                            <?php echo do_shortcode('[woosq id="'.$post_id.'"]'); ?>
                                        </a>
                                        <div class="ec-btn-group wishlist" title="Wishlist">
                                            <?php
                                                echo do_shortcode('[yith_wcwl_add_to_wishlist product_id="'.$post_id.'"]');
                                            ?>
                                        </div>
                                    </div>
	                            </div>
	                        </div>
	                        <div class="ec-pro-content">
					            <h5 class="ec-pro-title">
					            	<a href="<?php echo $post_link; ?>">
					            		<?php echo $post_title; ?>
					            	</a>
					            </h5>
					            <?php echo core_show_price_old_price($post_id); ?>
	                        </div>
	                    </div>
	                </div>
				<?php } ?>

            </div>
        	<?php } ?>
        </div>
    </div>
</section>

<?php if(!empty( $home_service_content )) { ?>
<section class="section ec-services-section section-space-p">
    <div class="container">
        <div class="row mb-minus-30">
			
			<?php
			    foreach ($home_service_content as $foreach_kq) {

			    $post_image = $foreach_kq["image"];
			    $post_title = $foreach_kq["title"];
			    $post_desc  = $foreach_kq["desc"];
			?>
	            <div class="ec_ser_content ec_ser_content_1 col-sm-12 col-md-4">
	                <div class="ec_ser_inner">
	                    <div class="ec-service-image">
	                        <img src="<?php echo $post_image; ?>" class="svg_img" alt="" />
	                    </div>
	                    <div class="ec-service-desc">
	                        <h2><?php echo $post_title; ?></h2>
	                        <p><?php echo $post_desc; ?></p>
	                    </div>
	                </div>
	            </div>
			<?php } ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="section ec-test-section section-space-ptb-100 section-space-mt section-space-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_testimonial_title; ?></h2>
                    <p class="sub-title"><?php echo $home_testimonial_desc; ?></p>
                </div>
            </div>
        </div>
        <div class="row">

        	<?php if(!empty( $home_testimonial_content )) { ?>
            <div class="ec-test-outer">
                <ul id="ec-testimonial-slider">
					
					<?php
					    foreach ($home_testimonial_content as $foreach_kq) {

					    $post_image = $foreach_kq["image"];
					    $post_title = $foreach_kq["title"];
					    $post_job  = $foreach_kq["job"];
					    $post_desc  = $foreach_kq["desc"];
					?>
	                    <li class="ec-test-item">
	                        <div class="ec-test-inner">
	                            <div class="ec-test-img">
	                            	<img alt="testimonial" title="testimonial" src="<?php echo $post_image; ?>" />
	                            </div>
	                            <div class="ec-test-content">
	                                <div class="ec-test-icon">
	                                	<img src="<?php echo core_asset('images/testimonial/quote-3.svg'); ?>" class="svg_img test_svg" alt="" />
	                                </div>
	                                <div class="ec-test-desc"><?php echo $post_desc; ?></div>
	                                <div class="ec-test-name"><?php echo $post_title; ?></div>
	                                <div class="ec-test-designation"><?php echo $post_job; ?></div>
	                            </div>
	                        </div>
	                    </li>
					<?php } ?>

                </ul>
            </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="section ec-instagram-section section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_instagram_title; ?></h2>
                    <p class="sub-title"><?php echo $home_instagram_desc; ?></p>
                </div>
                <div class="section-btn">
                    <span class="ec-section-btn">
                    	<a class="btn-secondary" href="<?php echo $home_instagram_url; ?>">
                    		<?php echo $home_instagram_button_text; ?>
                		</a>
                	</span>
                </div>
            </div>
        </div>
        <div class="row">

        	<?php if(!empty( $home_instagram_content )) { ?>
            <div class="ec-insta-wrapper">
                <div class="ec-insta-outer">
                    <div class="insta-auto">

						<?php
						    foreach ($home_instagram_content as $foreach_kq) {

						    $post_image = $foreach_kq["image"];
						    $post_link  = $foreach_kq["url"];
						?>
	                        <div class="ec-insta-item">
	                            <div class="ec-insta-inner">
	                                <a href="<?php echo $post_link; ?>" target="_blank">
	                                	<img src="<?php echo $post_image; ?>" alt="">
	                                </a>
	                            </div>
	                        </div>
						<?php } ?>

                    </div>
                </div>
            </div>
			<?php } ?>

        </div>
    </div>
</section>

<section class="section ec-home-news section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-title-block">
                <div class="section-title">
                    <h2 class="ec-title"><?php echo $home_news_title; ?></h2>
                    <p class="sub-title"><?php echo $home_news_desc; ?></p>
                </div>
                <div class="section-btn">
                    <span class="ec-section-btn">
                        <a class="btn-secondary" href="<?php echo $home_news_url; ?>">
                            <?php echo $home_news_button_text; ?>
                        </a>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">

            <?php if(!empty( $home_news_select_post )) { ?>
            <div class="ec-home-news-slider ec-trend-slider">

                <?php
                    foreach ($home_news_select_post as $foreach_kq) {

                    $post_id            = $foreach_kq->ID;
                    $post_title         = get_the_title($post_id);
                    $post_date          = get_the_date('d/m/Y',$post_id);
                    $post_link          = get_permalink($post_id);
                    $post_image         = core_getPostImage($post_id,"p-post");
                    $post_excerpt       = core_cut_string(get_the_excerpt($post_id),90,'...');
                    $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                    $post_tag           = get_the_tags($post_id);
                    $post_comment       = wp_count_comments($post_id);
                    $post_comment_total = $post_comment->total_comments;
                ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 blog-item">
                        <div class="blog-thumb">
                            <a href="<?php echo $post_link; ?>">
                                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                            </a>
                        </div>
                        <div class="blog-content">
                            <h5>
                                <a class="blog-title" href="<?php echo $post_link; ?>">
                                    <?php echo $post_title; ?>
                                </a>
                            </h5>
                            <div class="blog-meta mb-10">
                                <span>Bởi
                                    <a href="javascript:void(0)"><?php echo $post_author; ?> - </a>
                                </span>
                                <span><?php echo $post_date; ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <?php } ?>

        </div>
    </div>
</section>

<?php if(!empty( $home_partner_content )) { ?>
<section class="section ec-brand-section section-space-ptb-100 section-space-mt">
    <div class="container">
        <div class="row">
            <div class="ec-brand-outer">
                <ul class="ec-brand-inner">
				
					<?php
					    foreach ($home_partner_content as $foreach_kq) {

					    $post_image = $foreach_kq["image"];
					    $post_link  = $foreach_kq["url"];
					?>
	                    <li class="ec-brand-item col-lg-3 col-md-4 col-sm-4">
	                        <div class="ec-brand-img">
	                        	<a href="<?php echo $post_link; ?>">
	                        		<img alt="brand" title="brand" src="<?php echo $post_image; ?>" />
	                        	</a>
	                        </div>
	                    </li>
					<?php } ?>

                </ul>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function() {
	    $("#ec-offer-count").countdowntimer({
	        startDate: "<?php echo $home_date_here; ?>",
	        dateAndTime: "<?php echo $home_sale_date; ?>",
	        labelsFormat: true,
	        displayFormat: "DHMS"
	    });
	});
</script>