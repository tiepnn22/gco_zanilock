<?php get_header(); ?>

<?php
	$s = $_GET['s'];
    $cate = $_GET['cate'];

    $page_name = 'Tìm kiếm cho : ['.$s.']';
    $cat_id = (!empty($cate)) ? $cate : '';

    //banner
    $data_page_banner  = array(
        'image_alt'    =>    $page_name
    );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="ec-page-content-bnr section-space-pb">
    <div class="container">
        <div class="row">

            <div class="ec-shop-rightside col-lg-9 order-lg-last col-md-12 order-md-first margin-b-30">

                <div class="shop-pro-content">
                    <div class="shop-pro-inner">
                        <div class="row"> 

                            <?php
                                if( !empty($cat_id) ) {
                                    $query = core_query_search_post_by_taxonomy_paged($s, $cat_id, 'product', 'product_cat', 9);
                                } else {
                                    $query = core_query_search_post_paged($s, array('product'), 9);
                                }

                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $max_num_pages      = $query->max_num_pages;
                                $total_post         = $query->found_posts;
                                $total_post_start   = ($paged -1) * 9 + 1;
                                $total_post_end     = min( $total_post, $paged * 9 );

                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                            <!-- pagination -->
                            <nav class="ec-pro-pagination">
                                <span>Hiển thị <?php echo $total_post_start; ?> - <?php echo $total_post_end; ?> / <?php echo $total_post; ?> kết quả</span>
                                <?php echo core_paginationCustom( $max_num_pages ); ?>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="ec-shop-leftside col-lg-3 order-lg-first col-md-12 order-md-last">
                <div id="shop_sidebar">
                    <div class="ec-sidebar-heading">
                        <h1>Tìm kiếm sản phẩm</h1>
                    </div>

                    <div class="ec-sidebar-wrap">
                        <?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php get_footer(); ?>